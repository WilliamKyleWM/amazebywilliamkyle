package edu.wm.cs.cs301.WilliamKyle.gui;
import edu.wm.cs.cs301.WilliamKyle.generation.Factory;
import edu.wm.cs.cs301.WilliamKyle.generation.Maze;
import edu.wm.cs.cs301.WilliamKyle.generation.MazeBuilder;
import edu.wm.cs.cs301.WilliamKyle.generation.Order;
import edu.wm.cs.cs301.WilliamKyle.generation.MazeFactory;
import edu.wm.cs.cs301.WilliamKyle.generation.MazeBuilderEller;
import edu.wm.cs.cs301.WilliamKyle.generation.MazeBuilderPrim;

import androidx.appcompat.app.AppCompatActivity;
import edu.wm.cs.cs301.WilliamKyle.R;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.os.Bundle;
import android.transition.Slide;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View;
import android.content.Intent;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.TextView;
import android.widget.SeekBar;
import android.widget.CheckBox;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ProgressBar;
import android.os.Handler;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;


public class GeneratingActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener,Order {

    private Spinner robotSpinner;
    private int driverSelect;
    private RadioGroup grouping;
    private RadioButton wizard;
    private RadioButton wallFollow;
    private RadioButton manual;
    private String holder;
    private ProgressBar loadBar;
    private Button go;
    private int progressStat = 0;
    private Handler handler = new Handler();
    private static final String TAG = GeneratingActivity.class.getSimpleName();
    private String generation;
    private String rooms;
    private String diffVal;
    private TextView disText;
    private String orientation;
    private boolean man = true;
    protected Factory factory;
    private Thread orderThread;
    private Order order;
    private Maze mazeConfig;
    private int progress;
    private Singleton single;
    private int seed = 0;
    private String oldSeed;
    private Boolean oldSeedBool;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        getWindow().setExitTransition(new Slide());
        setContentView(R.layout.activity_generating);

        generation = getIntent().getStringExtra("Generation");
        rooms = getIntent().getStringExtra("Rooms");
        diffVal = getIntent().getStringExtra("DiffVal");

        oldSeed = getIntent().getStringExtra("Seed");
        if (oldSeed.equals("true")){
            oldSeedBool = true;
        }
        else{
            oldSeedBool = false;
        }

        loadBar = findViewById(R.id.progressBar);
        loadBar.setVisibility(View.VISIBLE);
        loadBar.setProgress(0);
        loadBar.setMax(100);

        factory = new MazeFactory();
        factory.order(this);

        /*
        new Thread(){
            public void run() {
                factory = new MazeFactory();
                factory.order(order);
                int progress = order.getPercent();
                while(progress < 100) {
                    if (order.getPercent() > progress) {
                        progress = order.getPercent();
                        loadBar.setProgress(progress);
                    }
                }
                factory.waitTillDelivered();
            }
        }.start();
*/

        /*
        Selection textviews by their id's
         */
        TextView rooms = (TextView) findViewById(R.id.rooms);
        TextView generation = (TextView) findViewById(R.id.generation);
        TextView diff = (TextView) findViewById(R.id.diff);


        disText = (TextView) findViewById(R.id.robot_sensor);


        generation.setText(" GENERATION ALGORITHM: "+getIntent().getStringExtra("Generation")+" ");
        rooms.setText(" ROOMS: "+getIntent().getStringExtra("Rooms")+" ");
        diff.setText(" DIFFICULTY: "+ getIntent().getStringExtra("DiffVal")+" ");

        robotSpinner = (Spinner) findViewById(R.id.robotSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.RobotSpinner, R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        robotSpinner.setAdapter(adapter);
        robotSpinner.setOnItemSelectedListener(this);

        grouping = (RadioGroup) findViewById(R.id.radioGroup);
        wizard = (RadioButton) findViewById(R.id.wizard);
        manual = (RadioButton) findViewById(R.id.manual);
        wallFollow = (RadioButton) findViewById(R.id.wallFollower);

        driverSelect = grouping.getCheckedRadioButtonId();
        grouping.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (wizard.isChecked()) {
                    holder = "Wizard";
                    man = false;
                    robotSpinner.setVisibility(View.VISIBLE);
                    disText.setVisibility(View.VISIBLE);
                }
                else if (manual.isChecked()){
                    holder = "Manual";
                    man = true;
                    robotSpinner.setVisibility(View.INVISIBLE);
                    disText.setVisibility(View.INVISIBLE);
                }
                else if (wallFollow.isChecked()){
                    holder = "Wall Follower";
                    man = false;
                    robotSpinner.setVisibility(View.VISIBLE);
                    disText.setVisibility(View.VISIBLE);
                }
                Toast.makeText(radioGroup.getContext(),holder,Toast.LENGTH_SHORT).show();
                Log.v(TAG, "Driver: "+String.valueOf(holder));
            }
        });

        go = (Button) findViewById(R.id.GO);
        go.setVisibility(View.VISIBLE);
        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mazeConfig != null) {
                    if (man) {
                        ManualTime();
                    } else {
                        AnimationTime();
                    }
                }
                else{
                    Toast.makeText(view.getContext(),"Maze not finished generating.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        /*
        Special thanks to 'Adobe in a minute' for the great tutorial https://www.youtube.com/watch?v=SaTx-gLLxWQ
         */

        new Thread(new Runnable(){
            @Override
            public void run() {
                while (progressStat<100){
                    progressStat+=1;
                    android.os.SystemClock.sleep(10000);
                    handler.post(new Runnable(){

                        @Override
                        public void run() {
                            loadBar.setProgress(progressStat);
                        }
                    });
                }
                handler.post(new Runnable(){

                    @Override
                    public void run() {
                        go.setVisibility(View.VISIBLE);
                    }
                });
            }

        }).start();

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        int position = adapterView.getPositionForView(view);
        orientation = adapterView.getItemAtPosition(position).toString();
        Toast.makeText(adapterView.getContext(),orientation, Toast.LENGTH_SHORT).show();
        Log.v(TAG, "Sensors setup: "+String.valueOf(orientation));
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    public void ManualTime(){
        Intent intent = new Intent(this, PlayManuallyActivity.class);

        intent.putExtra("Rooms",rooms);
        intent.putExtra("Generation",generation);
        intent.putExtra("DiffVal",diffVal);

        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }

    public void AnimationTime(){
        /*
        Starting intent
         */

        Intent intent = new Intent(this, PlayAnimationActivity.class);

        /*
        Using singleton to get instance and set values
         */
        intent.putExtra("Rooms",rooms);
        intent.putExtra("Generation",generation);
        intent.putExtra("DiffVal",diffVal);
        intent.putExtra("Driver",holder);
        intent.putExtra("Orientation",orientation);

        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }


    @Override
    public int getSkillLevel() {
        diffVal = getIntent().getStringExtra("DiffVal");
        int hold = Integer.parseInt(diffVal);
        return hold;
    }

    @Override
    public Builder getBuilder() {
        switch (generation) {
            case "Prim":
                return Builder.Prim;
            case "Eller":
                return Builder.Eller;
            
            default:
                return Builder.DFS;
        }
    }

    @Override
    public int getPercent() {
        return progress;
    }

    @Override
    public boolean isPerfect() {
        if (rooms.equals("true")) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int getSeed() {
        inputSeed();
        if (oldSeedBool) {
            return seed;
        }
        else{
            int random_int = (int)(Math.random() * 100);
            return random_int;
        }
    }

    @Override
    public void deliver(Maze mazeConfig) {
        this.mazeConfig = mazeConfig;
        single = Singleton.getInstance();
        single.setMazeConfig(mazeConfig);
    }

    @Override
    public void updateProgress(int percentage) {
        progress = percentage;
        loadBar.setProgress(progress);
    }

    private void inputSeed() {
        String string = "";

        FileInputStream fileInputStream = null;
        try {
            fileInputStream = openFileInput("seed");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            string = bufferedReader.readLine();

            Long temp = Long.parseLong(string);
            temp = temp % 100000;
            seed = Math.toIntExact(temp);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
