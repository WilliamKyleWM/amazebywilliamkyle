package edu.wm.cs.cs301.WilliamKyle.gui;

import androidx.appcompat.app.AppCompatActivity;
import edu.wm.cs.cs301.WilliamKyle.R;
import edu.wm.cs.cs301.WilliamKyle.generation.CardinalDirection;
import edu.wm.cs.cs301.WilliamKyle.generation.Maze;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.Switch;
import android.widget.TextView;
import android.view.View;
import android.content.Intent;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.TextView;
import android.widget.SeekBar;
import android.widget.CheckBox;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ProgressBar;
import android.os.Handler;
import android.widget.ImageView;

public class PlayAnimationActivity extends AppCompatActivity {

    public ReliableRobot robot;
    public RobotDriver driver;

    private Handler handler = new Handler();

    private String drover;
    private TextView generation;
    private TextView rooms;
    private TextView diff;
    private Button winning;
    private Button losing;
    private Integer odometer;
    public float battery_level = EnergyCost.starting_level;
    public boolean stopped;
    private String orientation;
    private boolean pause = true;

    static StatePlayingAnimation currentState;

    private SeekBar speed;

    private Button mazeView;
    private Button solution;
    private Button walls;

    private Button stopGo;

    private Button plus;
    private Button minus;

    static MazePanel panel;

    private ImageView shaky;
    private ImageView premium;
    private ImageView soso;
    private ImageView mediocre;
    private boolean outside;

    goRobot animation;

    Maze mazeConfig;

    private static final String TAG = PlayManuallyActivity.class.getSimpleName();

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_animation);

    /** setting up mazepanel*/

        panel = new MazePanel(this);
        panel = findViewById(R.id.panelanimation);
        Singleton.setMazePanel(panel);

        winning = (Button) findViewById(R.id.winning);
        losing = (Button) findViewById(R.id.losing);
        winning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToWinning();
            }
        });
        losing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToLosing();
            }
        });

        /**
         * sensor layout set up
         */

        shaky = (ImageView) findViewById(R.id.shaky);
        soso = (ImageView) findViewById(R.id.soso);
        mediocre = (ImageView) findViewById(R.id.mediocre);
        premium = (ImageView) findViewById(R.id.premium);
        orientation = getIntent().getStringExtra("Orientation");
        Toast.makeText(this,orientation,Toast.LENGTH_SHORT).show();
        Log.v(TAG, orientation);


        if (orientation.equals("SoSo")){
            soso.setVisibility(View.VISIBLE);
            shaky.setVisibility(View.INVISIBLE);
            mediocre.setVisibility(View.INVISIBLE);
            premium.setVisibility(View.INVISIBLE);
        }
        else if (orientation.equals("Premium")){
            premium.setVisibility(View.VISIBLE);
            shaky.setVisibility(View.INVISIBLE);
            mediocre.setVisibility(View.INVISIBLE);
            soso.setVisibility(View.INVISIBLE);
        }
        else if (orientation.equals("Mediocre")){
            mediocre.setVisibility(View.VISIBLE);
            shaky.setVisibility(View.INVISIBLE);
            premium.setVisibility(View.INVISIBLE);
            soso.setVisibility(View.INVISIBLE);
        }
        else if (orientation.equals("Shaky")){
            shaky.setVisibility(View.VISIBLE);
            premium.setVisibility(View.INVISIBLE);
            mediocre.setVisibility(View.INVISIBLE);
            soso.setVisibility(View.INVISIBLE);
        }

        /**
         * Zoom in and out
         */
        plus = (Button) findViewById(R.id.plus);
        minus = (Button) findViewById(R.id.minus);

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v(TAG, "Zoom in");
                currentState.keyDown(Constants.UserInput.ZoomIn,0);
                //Toast.makeText(view.getContext(), "Zoom in", Toast.LENGTH_SHORT).show();
            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v(TAG, "Zoom out");
                currentState.keyDown(Constants.UserInput.ZoomOut,0);
                //Toast.makeText(view.getContext(), "Zoom out", Toast.LENGTH_SHORT).show();
            }
        });

        stopGo = findViewById(R.id.stopGo);
        stopGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animation.run();
                if (outside) {
                    goToWinning();
                }
            }
        });

        /**
         * Toggles maze
         */
        /**
        For showing the map
         */
        mazeView = (Button) findViewById(R.id.mazetoggle);
        solution = (Button) findViewById(R.id.solution);

        mazeView.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentState.keyDown(Constants.UserInput.ToggleLocalMap,1);
                currentState.draw();
                Log.v(TAG, "mazeMap shown");
            }
        });

        solution.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentState.keyDown(Constants.UserInput.ToggleSolution,1);
                currentState.draw();
                Log.v(TAG, "solution shown");
            }
        });


        speed = (SeekBar) findViewById(R.id.speed);
        speed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.v(TAG, "Speed selected: "+String.valueOf(seekBar.getProgress()));
                Toast.makeText(seekBar.getContext(),"Speed: " +String.valueOf(seekBar.getProgress()),Toast.LENGTH_SHORT).show();
            }
        });


        /**
         Setting up robot and driver
         */
        Singleton singleton = Singleton.getInstance();
        mazeConfig = singleton.getMazeConfig();
        currentState = new StatePlayingAnimation();

        robot = new ReliableRobot(currentState);
        drover = getIntent().getStringExtra("Driver");
        if (drover.equals("Wall Follower")){
            driver = new WallFollower();
        }
        else if (drover.equals("Wizard")){
            driver = new Wizard();
        }
        else{
            Log.v("PlayAnimationActivity","driver not selected correctly");
        }
        driver.setRobot(robot);
        driver.setMaze(mazeConfig);

        currentState.setMazeConfiguration(mazeConfig);
        currentState.start(this,panel);

        animation = new goRobot(robot,driver);


        /*getting maze to show on panel*/

        //panel.invalidate();

    }

    public Maze getMazeConfiguration() {
        return mazeConfig;
    }

    public void win() {
        Intent intent = new Intent(this, WinningActivity.class);
        intent.putExtra("odometer",odometer);
        intent.putExtra("batteryLevel",battery_level);
        startActivity(intent);
    }

    //Solve the maze!
    class goRobot{
        private Robot robot;
        private RobotDriver driver;
        goRobot(Robot r, RobotDriver d) {
            robot = r;
            driver = d;
        }

        public void run() {
                try {
                    driver.drive1Step2Exit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //android.os.SystemClock.sleep(1000);
                currentState.draw();
            if (currentState.isOutside(currentState.px,currentState.py)) {
                outside = true;
                //goToWinning();
            }
                /*handler.post(new Runnable(){
                    @Override
                    public void run() {
                        currentState.draw();
                    }
                });
                //android.os.SystemClock.sleep(1);
            */

        }
    }



    private void goToWinning(){
        Intent intent = new Intent(this, WinningActivity.class);
        intent.putExtra("odometer",robot.getOdometerReading());
        intent.putExtra("batteryLevel",robot.getBatteryLevel());
        startActivity(intent);
    }

    private void goToLosing(){
        Intent intent = new Intent(this, LosingActivity.class);
        intent.putExtra("odometer",robot.getOdometerReading());
        intent.putExtra("batteryLevel",robot.getBatteryLevel());
        startActivity(intent);
    }


    public void switchFromPlayingToWinning(int i) {
        Intent intent = new Intent(this, WinningActivity.class);
        intent.putExtra("odometer",robot.getOdometerReading());
        intent.putExtra("batteryLevel",robot.getBatteryLevel());
        startActivity(intent);
    }

    public void switchToTitle() {
        Intent intent = new Intent(this, AMazeActivity.class);
        startActivity(intent);
    }
}