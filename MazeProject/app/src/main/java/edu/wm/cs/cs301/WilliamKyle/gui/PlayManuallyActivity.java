package edu.wm.cs.cs301.WilliamKyle.gui;

import androidx.appcompat.app.AppCompatActivity;
import edu.wm.cs.cs301.WilliamKyle.R;
import edu.wm.cs.cs301.WilliamKyle.generation.Maze;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageButton;
import android.widget.Switch;

public class PlayManuallyActivity<single> extends AppCompatActivity {
    public float battery_level = EnergyCost.starting_level;
    public boolean stopped;
    private TextView generation;
    private TextView diff;
    private TextView rooms;
    private Button winning;
    private Button losing;
    private ImageButton arrowup;
    private ImageButton arrowleft;
    private ImageButton arrowright;
    static MazePanel panel;
    private Singleton single;
    static StatePlaying currentState;
    private Integer mapscale;

    private Button mazeView;
    private Button solution;
    private Button walls;

    private Button plus;
    private Button minus;
    Maze mazeConfig;

    private static final String TAG = PlayManuallyActivity.class.getSimpleName();
    private boolean mapView;
    private int steps;
    private float battery_battery = EnergyCost.starting_level;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_manually);

        /*setting up mazepanel!*/

        panel = new MazePanel(this);
        panel = findViewById(R.id.manualpanel);
        Singleton.setMazePanel(panel);

        //generation = (TextView) findViewById(R.id.generation);
        //diff = (TextView) findViewById(R.id.diff);
        //ooms = (TextView) findViewById(R.id.rooms);
/*
        generation.setText(" GENERATION ALGORITHM: "+getIntent().getStringExtra("Generation")+" ");
        rooms.setText(" ROOMS: "+getIntent().getStringExtra("Rooms")+" ");
        diff.setText(" DIFFICULTY: "+ getIntent().getStringExtra("DiffVal")+" ");
        */


        /**
         * Switches at the top
         */
        mapscale = 25;
        mazeView = (Button) findViewById(R.id.mazetoggle);
        solution = (Button) findViewById(R.id.solution);



        /*
        For showing the map
         */
        mazeView.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentState.keyDown(Constants.UserInput.ToggleLocalMap,1);
                currentState.draw();
                Log.v(TAG, "mazeMap shown");
            }
            });

        solution.setOnClickListener(new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            currentState.keyDown(Constants.UserInput.ToggleSolution,1);
            currentState.draw();
            Log.v(TAG, "solution shown");
    }
    });

        /**
         * Zoom in and out
         */
        plus = (Button) findViewById(R.id.plus);
        minus = (Button) findViewById(R.id.minus);

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v(TAG, "Zoom in");
                mapscale = mapscale + 10;
                currentState.keyDown(Constants.UserInput.ZoomIn,0);
                //Toast.makeText(view.getContext(), "Zoom in", Toast.LENGTH_SHORT).show();
            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapscale = mapscale -10;
                Log.v(TAG, "Zoom out");
                currentState.keyDown(Constants.UserInput.ZoomOut,0);
                //Toast.makeText(view.getContext(), "Zoom out", Toast.LENGTH_SHORT).show();
            }
        });

        /**
         * Arrows for movement
         */

        arrowup = (ImageButton) findViewById(R.id.arrowUp);

        arrowleft = (ImageButton) findViewById(R.id.arrowLeft);

        arrowright = (ImageButton) findViewById(R.id.arrowRight);
        /*moving forward*/
        final MediaPlayer player = MediaPlayer.create(this, R.raw.step);
        arrowup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentState.walk(1);
                //Toast.makeText(view.getContext(),"UP clicked", Toast.LENGTH_SHORT).show();
                Log.v(TAG, String.valueOf("UP clicked"));
                /* sound effect move forward */
                player.start();
                steps++;
                battery_battery = battery_battery-EnergyCost.forward;
                if (currentState.isOutside(currentState.px,currentState.py)) {
                    goToWinning();
                }
                    /*

                player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        player.release();
                    }
                });*/

            }
        });
        arrowleft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentState.rotate(1);
                //Toast.makeText(view.getContext(),"LEFT clicked", Toast.LENGTH_SHORT).show();
                Log.v(TAG, String.valueOf("LEFT clicked"));
                battery_battery = battery_battery-EnergyCost.rotate;
            }
        });
        arrowright.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                currentState.rotate(-1);
                //Toast.makeText(view.getContext(),"RIGHT clicked", Toast.LENGTH_SHORT).show();
                Log.v(TAG, String.valueOf("RIGHT clicked"));
                battery_battery = battery_battery-EnergyCost.rotate;
            }
        });

        Singleton singleton = Singleton.getInstance();

        mazeConfig = singleton.getMazeConfig();
        currentState = new StatePlaying();
        currentState.setMazeConfiguration(mazeConfig);
        currentState.start(this,panel);
        panel.invalidate();
    }

    private void mazeClick(){
        currentState.keyDown(Constants.UserInput.ToggleLocalMap,0);
        Log.v(TAG, "mazeMap shown");
    }

    private void goToWinning(){
        Intent intent = new Intent(this, WinningActivity.class);
        intent.putExtra("odometer",steps);
        intent.putExtra("battery",battery_battery);
        startActivity(intent);
    }

    private void goToLosing(){
        Intent intent = new Intent(this, LosingActivity.class);
        startActivity(intent);
    }

    public void switchFromPlayingToWinning(int i) {
        Intent intent = new Intent(this, WinningActivity.class);
        intent.putExtra("odometer",steps);
        intent.putExtra("battery",battery_battery);
        startActivity(intent);
    }

    public void switchToTitle() {
        Intent intent = new Intent(this, AMazeActivity.class);
        startActivity(intent);
    }
}