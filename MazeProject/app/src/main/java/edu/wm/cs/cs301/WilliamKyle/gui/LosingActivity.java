package edu.wm.cs.cs301.WilliamKyle.gui;

import androidx.appcompat.app.AppCompatActivity;
import edu.wm.cs.cs301.WilliamKyle.R;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.media.MediaPlayer;


public class LosingActivity extends AppCompatActivity {
    private Button home;
    private Switch mazeView;
    private Switch solution;
    private Switch walls;

    private static final String TAG = PlayManuallyActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_losing);

        MediaPlayer crash = MediaPlayer.create(LosingActivity.this,R.raw.crash);
        crash.start();

        home = findViewById(R.id.home);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backHome();
            }
        });





    }

    private void backHome(){
        Intent intent = new Intent(this, AMazeActivity.class);
        startActivity(intent);
    }
    }
