package edu.wm.cs.cs301.WilliamKyle.gui;
import edu.wm.cs.cs301.WilliamKyle.generation.Maze;

//Special thanks to https://www.tutorialspoint.com/how-to-use-singleton-class-in-android :)


public class Singleton {
    public static Maze mazeConfig;
    private static final Singleton instance = new Singleton();
    public String sensors;
    public static MazePanel panel;
    Boolean efficient;
    private int mapScale;

    public static Singleton getInstance() {
        return instance;
    }

    public static void setMazeConfig(Maze maze){
        mazeConfig = maze;
    }

    public static void setMazePanel(MazePanel p){
        panel = p;
    }

    public static MazePanel getMazePanel(){
        return panel;
    }

    public static Maze getMazeConfig(){
        return mazeConfig;
    }


}
