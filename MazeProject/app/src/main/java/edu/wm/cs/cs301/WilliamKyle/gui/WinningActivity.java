package edu.wm.cs.cs301.WilliamKyle.gui;

import androidx.appcompat.app.AppCompatActivity;
import edu.wm.cs.cs301.WilliamKyle.R;

import android.media.MediaPlayer;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.os.Bundle;
import android.content.Intent;
import android.widget.TextView;

public class WinningActivity extends AppCompatActivity {
    Button home;
    private int steps;
    private float battery_level;
    TextView odometer;
    TextView battery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winning);
        steps = getIntent().getIntExtra("odometer",100);
        battery_level = getIntent().getFloatExtra("battery",EnergyCost.starting_level);
        /*setting text for end screen*/

        odometer = findViewById(R.id.odometer);
        battery = findViewById(R.id.battery_level);

        odometer.setText("Odometer: "+ steps);
        battery.setText("Battery level: "+ battery_level);

        home = findViewById(R.id.home);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backHome();
            }
        });


        final MediaPlayer player = MediaPlayer.create(this, R.raw.congrats);
        player.start();
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                player.release();
            }
        });
    }

    private void backHome(){
        Intent intent = new Intent(this, AMazeActivity.class);
        startActivity(intent);
    }


}