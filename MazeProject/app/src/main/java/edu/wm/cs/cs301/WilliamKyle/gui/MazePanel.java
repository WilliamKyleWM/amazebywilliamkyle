package edu.wm.cs.cs301.WilliamKyle.gui;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.solver.widgets.Rectangle;
import edu.wm.cs.cs301.WilliamKyle.generation.Maze;
import edu.wm.cs.cs301.WilliamKyle.generation.Wall;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.fonts.Font;
import android.graphics.fonts.FontStyle;
import android.graphics.Path;
import android.graphics.Rect;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/*
Big thanks to "Coderz Geek" for help getting started with View.
 */

public class MazePanel extends View implements P5Panel {
    private int int_col;
    private Context context;

    private AttributeSet attrs;
    private Color col;
    private Canvas canvas;
    private Paint paint;
    private Bitmap bitmap;
    private Config config;
    private Path path;
    private int viewWidth = 100;
    private int viewHeight = 100;
    //(Makes graphics magnified)
    final double add = 3.6;
    PlayManuallyActivity manually;
    GeneratingActivity generating;


    static final Color greenWM = Color.valueOf(17,87,64);
    static final Color goldWM = Color.valueOf(145,111,65);
    static final Color yellowWM = Color.valueOf(255,255,153);

    public MazePanel(Context context) {
        super(context);
        this.context = context;
        bitmap = Bitmap.createBitmap(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT, Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bitmap);
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
    }

    public MazePanel(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.attrs = attrs;
        init(attrs);
    }

    public MazePanel(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(attrs);
    }

    public MazePanel(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        init(attrs);
    }



    private void init(@Nullable AttributeSet set) {
        bitmap = Bitmap.createBitmap(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT, Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bitmap);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
    }


    @Override
    public void onMeasure(int width, int height) {
        // as described for superclass method
        super.onMeasure(width, height);
    }



    @Override
    protected void onDraw(Canvas can) {
        super.onDraw(can);
        if (canvas == null){
            bitmap = Bitmap.createBitmap(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT, Bitmap.Config.ARGB_8888);
            canvas = new Canvas(bitmap);
            paint = new Paint();
            paint.setStyle(Paint.Style.FILL);
        }
        else if (bitmap == null){
            bitmap = Bitmap.createBitmap(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT, Bitmap.Config.ARGB_8888);
            canvas = new Canvas(bitmap);
            paint = new Paint();
            paint.setStyle(Paint.Style.FILL);
        }
        //myTestImage();
        can.drawBitmap(bitmap,0,0,null);
    }

    private void myTestImage(){
        setColor(Color.GREEN);
        addFilledRectangle(10,10,100,100);
        setColor(Color.RED);
        addFilledOval(100,100,200,200);
        setColor(Color.BLUE);
        addLine(200,200,400,400);
    }

    @Override
    public void commit() {
        //canvas.drawBitmap(bitmap,0,0,paint);
        invalidate();
    }

    @Override
    public boolean isOperational() {
        return false;
    }

    /**
     * Methods from MazePanel
     */
    @Override
    public void setColor(int rgb) {
        // TODO Auto-generated method stub
        int_col = rgb;
        paint.setColor(rgb);
        //paint.setColor()
    }

    private void setColorCol(Color col){
        this.col = col;

    }
    @Override
    public int getColor() {
        // TODO Auto-generated method stub
        return int_col;
    }

    private Color getCol() {
        return col;
    }


    static public int getWallColor(int distance, int cc, int extensionX) {
        // TODO Auto-generated method stub
        // mod used to limit the number of colors to 6
        int d = distance;
        final int rgbValue = calculateRGBValue(d, extensionX);
        return rgbValue;
    }

    @Override
    public void addBackground(float percentToExit) {
        if (canvas == null){
            Log.v("add background","null canvas");
            invalidate();
        }
        Rect top = new Rect(0,0,1440,720);
        Paint pTop = new Paint();
        int blackReplacement = Color.parseColor("#00303D");
        pTop.setColor(blackReplacement);

        Rect bottom = new Rect(0, 720, 1440, 1440);
        Paint pBottom = new Paint();
        pBottom.setColor(Color.GRAY);


        canvas.drawRect(bottom, pBottom); // bottom

        //top
        canvas.drawRect(top, pTop);
        /*
            // TODO Auto-generated method stub
            setColorCol(getBackgroundColor(percentToExit, true));
            addFilledRectangle(0, 0, canvas.getWidth(), canvas.getHeight()/2);
            // grey rectangle in lower half of screen
            // graphics.setColor(Color.darkGray);
            // dynamic color setting:
            setColorCol(getBackgroundColor(percentToExit, false));
            addFilledRectangle(0, 1000, canvas.getWidth(), canvas.getHeight()/2);*/
    }
    @Override
    public void addFilledRectangle(int x, int y, int width, int height) {
        // TODO Auto-generated method stub
        //addFilledRectangle(0, 0, width, height / 2);
        if (canvas == null){
            Log.v("add background","null canvas");

        }
        Rect rect = new Rect();
        rect.left = x;
        rect.top = y;
        rect.right = rect.left+width;
        rect.bottom = rect.top+height;
        canvas.drawRect(rect, paint);

        //graphics.fillRect(0, 0, width, height/2);
    }

    //Thanks!
    //https://stackoverflow.com/questions/2047573/how-to-draw-filled-polygon
    @Override
    public void addFilledPolygon(int[] xPoints, int[] yPoints, int nPoints) {
        // TODO Auto-generated method stub
        Path path = new Path();
        path.moveTo(xPoints[0], yPoints[0]);
        path.lineTo(xPoints[1], yPoints[1]);
        path.lineTo(xPoints[2], yPoints[2]);
        path.lineTo(xPoints[3], yPoints[3]);
        path.lineTo(xPoints[0], yPoints[0]);
        canvas.drawPath(path, paint);
        }

        /**
         * Add polygon, very similar to Java project
         * @param xPoints
         * @param yPoints
         * @param nPoints
         */
        @Override
    public void addPolygon(int[] xPoints, int[] yPoints, int nPoints) {
        // TODO Auto-generated method stub
        Path path = new Path();
        path.reset(); // only needed when reusing this path for a new build
        path.moveTo(xPoints[0], yPoints[0]); // used for first point
        for (int i = 1;i<nPoints;i++) {
            path.lineTo(xPoints[i], yPoints[i]);
        }
        path.lineTo(xPoints[0], yPoints[0]);
        canvas.drawPath(path, paint);
    }
    @Override
    public void addLine(int startX, int startY, int endX, int endY) {
        // TODO Auto-generated method stub
        //addLine(startX,startY,endX,endY);
        canvas.drawLine(startX,startY,endX,endY,paint);

        }

        /**
         * Oval :)
         * @param x
         * @param y
         * @param w
         * @param height
         */
        @Override
    public void addFilledOval(int x, int y, int w, int height) {
        // TODO Auto-generated method stub
            //addFilledOval(x,y,w,height);
        canvas.drawOval(x,y,w,height,paint);
        }

    /**
     * Arc :)
     * @param x
     * @param y
     * @param width
     * @param height
     * @param startAngle
     * @param arcAngle
     */
    @Override
    public void addArc(int x, int y, int width, int height, int startAngle, int arcAngle) {
        addArc(x,y,width,height,startAngle,arcAngle);
            // TODO Auto-generated method stub
        canvas.drawArc(x,y,width,height,startAngle,arcAngle,true,paint);
        /**
         * Use center boolean, make sure that's right
         */
    }

    @Override
    public void addMarker(float x, float y, String str) {
        if (canvas == null){
            Log.v("addMarker","null canvas");
        }
        canvas.drawText(str,x,y,paint);
    }


    /**
     * Not really needed
     * @param hintKey the key of the hint to be set.
     * @param hintValue the value indicating preferences for the specified hint category.
     */
    @Override
    public void setRenderingHint(P5Panel.RenderingHints hintKey, P5Panel.RenderingHints hintValue) {

    }


    /**
     * GetbackgroundColor method from FirstPersonView
     * @param percentToExit
     * @param top
     * @return
     */
    private Color getBackgroundColor(float percentToExit, boolean top) {
        return top? blend(yellowWM, goldWM, percentToExit) :
                blend(Color.valueOf(Color.LTGRAY), greenWM, percentToExit);
    }


    /**
     * Color blend from FirstPersonView
     * @param c0
     * @param c1
     * @param weight0
     * @return
     */
    private Color blend(Color c0, Color c1, float weight0) {
        if (weight0 < 0.1)
            return c1;
        if (weight0 > 0.95)
            return c0;
        float r = weight0 * c0.red() + (1-weight0) * c1.red();
        float g = weight0 * c0.green() + (1-weight0) * c1.green();
        float b = weight0 * c0.blue() + (1-weight0) * c1.blue();
        float a = Math.max(c0.alpha(), c1.alpha());
        return Color.valueOf(r,g,b,a);
    }

    /**
     * From Wall!
     * Computes an RGB value based on the given numerical value.
     *
     * @param distance
     *            value to select color
     * @return the calculated RGB value
     */
    private static int calculateRGBValue(final int distance, int extensionX) {
        // compute rgb value, depends on distance and x direction
        // 7 in binary is 0...0111
        // use AND to get last 3 digits of distance
        final int part1 = distance & 7;
        final int add = (extensionX != 0) ? 1 : 0;
        final int rgbValue = ((part1 + 2 + add) * 70) / 8 + 80;
        return rgbValue;
    }


    /**
     * Converts int array into normal integer!
     * Idea from https://www.shodor.org/stella2java/rgbint.html
     * @param red
     * @param blue
     * @param green
     * @return
     */
    static public int colorSwitch(int red, int blue, int green) {
        return 256*256*red+256*green+blue;
    }

    /**
     * stores fields into the given document with the help of MazeFileWriter.
     *
     * @param doc
     *            document to add data to
     * @param mazeXML
     *            element to add data to
     * @param number
     *            number for this element
     * @param i
     *            id for this element
     */
    public void storeWallPanel(final Document doc, final Element mazeXML,
                               final int number, final int i) {
        MazeFileWriter.appendChild(doc, mazeXML, "colSeg_" + number + "_" + i,
                getColor());
    }

    public void setColorFloat(float[] f) {
        paint.setColor(Color.LTGRAY);
    }

    public void paintComponent(MazePanel panel) {
        // TODO Auto-generated method stubdfd

    }


}
