package edu.wm.cs.cs301.WilliamKyle.gui;

import androidx.appcompat.app.AppCompatActivity;
import edu.wm.cs.cs301.WilliamKyle.R;

import android.app.ActivityOptions;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Slide;
import android.view.View;
import android.content.Intent;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.TextView;
import android.widget.SeekBar;
import android.widget.CheckBox;
import android.widget.Button;
import android.util.Log;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class AMazeActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private Spinner spinner;
    private SeekBar seekbar;
    private TextView difficulty;
    private CheckBox checkBox;
    private int diffVal = 1;
    private boolean rooms = false;
    private Button explore;
    private Button revisit;
    private static String generation = "DFS";
    private static final String TAG = AMazeActivity.class.getSimpleName();
    private static final String level = "com.example.williamkyle.amazebywilliamkyle.level";
    private static final String seed = "com.example.williamkyle.amazebywilliamkyle.seed";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        /**
         * Music added!
         */
        final MediaPlayer player = MediaPlayer.create(this, R.raw.music);
        player.start();
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                player.release();
            }
        });

        /*
        Spinner selection
         */
        spinner = findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.MazeSelection, R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);


        getWindow().setExitTransition(new Slide());

        revisit = (Button) findViewById(R.id.revisit);
        revisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "Revisit", Toast.LENGTH_SHORT).show();
                Log.v(TAG, "Revisit button clicked");
                revisitTime(diffVal,rooms,generation);
            }
        });

        /*
        Setting up buttons for explore
         */
        explore = (Button) findViewById(R.id.explore);
        explore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generateTime(diffVal,rooms,generation);
            }
        });

        checkBox = (CheckBox) findViewById(R.id.checkBox);
        checkBox.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                rooms = b;
                Toast.makeText(compoundButton.getContext(), "Rooms: "+b+"", Toast.LENGTH_SHORT).show();
                Log.v(TAG, "Rooms? "+String.valueOf(b));
            }
        });

        difficulty = (TextView) findViewById(R.id.difficulty);
        seekbar = (SeekBar) findViewById(R.id.seekBar);
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            difficulty.setText(getString(R.string.newdif)+i+" ");
            diffVal = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            Toast.makeText(seekBar.getContext(),"Difficulty: "+diffVal+"",Toast.LENGTH_SHORT).show();
            Log.v(TAG, "Difficulty selected "+String.valueOf(diffVal));
            }
        });
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        int position = adapterView.getPositionForView(view);
        generation = adapterView.getItemAtPosition(position).toString();
        String text = adapterView.getItemAtPosition(position).toString();
        Toast.makeText(adapterView.getContext(),text, Toast.LENGTH_SHORT).show();
        Log.v(TAG, "Gen selected "+text);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void generateTime(int diffVal, boolean rooms, String generation){
        ioSeed();
        Intent intent = new Intent(this, GeneratingActivity.class);
        String roomString = ""+rooms+"";
        String diffValString = ""+diffVal+"";
        intent.putExtra("Rooms",roomString);
        intent.putExtra("Generation",generation);
        intent.putExtra("DiffVal",diffValString);
        intent.putExtra("Seed","false");
        //startActivity(intent);
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }

    public void revisitTime(int diffVal, boolean rooms, String generation){
        ioSeed();
        Intent intent = new Intent(this, GeneratingActivity.class);
        String roomString = ""+rooms+"";
        String diffValString = ""+diffVal+"";
        intent.putExtra("Rooms",roomString);
        intent.putExtra("Generation",generation);
        intent.putExtra("DiffVal",diffValString);
        intent.putExtra("Seed","true");
        //startActivity(intent);
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }

    private void ioSeed() {
        String string = "" + System.currentTimeMillis();
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = openFileOutput(seed, MODE_PRIVATE);
            fileOutputStream.write(string.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != fileOutputStream) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}